public class CountNines {
	public static int getCountNines(int a, int b) {
		if (a < 0 || b < 0) {
            return -1;
        }
		if (a==0 || b==0){
			return -2;
		}

        int numberOfNines = 0;
        int numberOfDigits = Integer.toString(b).length();
        for (int i = a; i < b; i++) {
            int currentNumber = i;
            for (int j = 0; j < numberOfDigits; j++) {
                if (currentNumber % 10 == 9) {
                    numberOfNines++;
                }
                currentNumber -= currentNumber % 10;
                currentNumber /= 10;
            }
            if(numberOfNines == 0)
            	return -3;
        }
        

        return numberOfNines;
    }
	

	public static void main(String[] args) {
		System.out.println(getCountNines(30, 20));

	}

}
public class OddPalindromes01 {
	public static void main(String[] args) {
		int num1 = 1500;
		int num2 = 2000;
		System.out.println(generateOddPalindromes(num1, num2));
	}

	public static String generateOddPalindromes(int start, int limit) {
		String res="";
		if(start<=0||limit<=0)
			return "-1";
		else if(start>=limit)
			return "-2";
		else{
			for(int i=start;i<=limit;i++){
				if(isPalindrome(i)&&isAllDigitsOdd(i)){
					res+=i+",";

				}
			}
		}
		if(res.isEmpty())
			return "-3";
		
		return res.substring(0, res.length()-1);
	}

	public static boolean isPalindrome(int num) {
		return num==reverse(num);
	}

	public static int reverse(int num) {
        int rev=0;
        while(num>0){
        	int digit=num%10;
        	rev=digit+rev*10;
        	num/=10;
            
        }
        return rev;
        
	}

	public static boolean isAllDigitsOdd(int num) {
		while (num > 0) {
	           int digit = num % 10;
				if (digit != 0 && digit % 2 == 0 )
					return false;
				num = num / 10;
			}
			return true;
	}
}


import java.util.Scanner;

public class Collatz01 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int num= sc.nextInt();
		System.out.println(getCollatzSequence(num));
	}
	public static String getCollatzSequence(int number) {
		int count=0;
		String s ="",t="";
		if(number<=0)
			return"Error";
		if(number==1)
			return "1 4 2 1";
		if(number==2)
			return "2 1 4 2 1";

		while(number!=1)
		{
			s= s+number+" ";
			if(number%2==0){
				number/=2;
			}else{
				number=(number*3)+1;
			}
			count++;
			if(number==1)
				s +="1"; 
			if (count>=100 && number!=1)
				return " Does not Converge ";
		}    	
		return s;
	}
}



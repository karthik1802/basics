package day12;

import java.util.Scanner;

public class StrongNumber {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int num=sc.nextInt();
		System.out.println(isStrong(num));

	}

	private static boolean isStrong(int num) {
		return num==sumOfDigitFactorial(num);

	}

	private static int sumOfDigitFactorial(int num) {
		int sum=0;int digit=0,temp = 0;
		while (num>0){
			digit = num%10;
			temp=fact(digit);
			sum +=temp;
			num=num/10;
		}


		return sum;
	}

	private static int fact(int num) {
		int fact = 1;
		if(num>0){
			fact=num*fact(num-1);

		}
		//System.out.println(fact);
		return fact;
	}

}

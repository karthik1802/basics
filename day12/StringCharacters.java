package day12;

import java.util.Scanner;

public class StringCharacters {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println(("Enter a String: "));
		String str = sc.nextLine();
		charCheck(str);
	}

	private static void charCheck(String str) {
		int vowelcount=0,con=0,digit=0;
		String nextChar="";
		for(int i=0;i<str.length();i++){
			char c=str.charAt(i);
			int ascii = Character.valueOf(c);
			ascii++;
			nextChar+=(char)ascii;
		}
		str=str.toLowerCase();
		for(int i=0;i<str.length();i++){
			char c = str.charAt(i);
			
			if(Character.isDigit(c))
				digit++;
			else if(c=='a'||c=='e'||c=='i'||c=='o'||c=='u'){
				vowelcount++;
			}else con++;
		}
		System.out.println("Vowelcount : " + vowelcount);
		System.out.println("Consonantcount : " + con);
		System.out.println("Digitscount : " + digit);
		System.out.println(nextChar);
		
	}

}

package day1;

import java.util.Scanner;

public class Operations1 {

	public static void main(String[] args) {

		System.out.println("Enter first number");
		
		Scanner scan= new Scanner(System.in);
		int num1=scan.nextInt();
		
		System.out.println("Enter second number");
		int num2=scan.nextInt();
		
		int sum= num1 + num2;
		int diff=num2 - num1;
		int mul=num1 * num2;
		float div=num1 / num2;
		int mod= num1%num2;

		System.out.println("Sum of "+num1+","+ num2+" = "+sum);

		System.out.println("Difference of "+num1+","+ num2+"= "+diff);

		System.out.println("Multiplication of "+num1+","+ num2+" = "+mul);

		System.out.println("Division of "+num1+","+ num2+"= "+div);

		System.out.println("Modulus of "+num1+","+ num2+"= "+mod);


	}

}

package day1;

public class Swap1 {
	public static void main(String[] args) {
		int num1,num2;
		num1=2;
		num2=6;
		System.out.println("Before swapping is "+ num1+" " +num2);
		num1=num1+num2;
		num2=num1-num2;
		num1=num1-num2;
		System.out.println("After swapping is "+ num1+" " +num2);
	}

}

package day1;

public class Swap {
 
	public static void main(String[] args) {
		int num1,num2,temp=0;
		num1=2;
		num2=6;
		System.out.println("Before swapping is "+ num1+" " +num2);
		temp=num1;
		num1=num2;
		num2=temp;
		System.out.println("After swapping is "+ num1+" " +num2);
	}

}

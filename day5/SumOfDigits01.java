package day5;

public class SumOfDigits01 {
    public static void main(String[] args) {
        int num = 799;
        System.out.println(getSumOfDigits(num));
    }

    public static int getSumOfDigits(int num)	{
    	int a,b,sum;
    	if(num>=10 && num<=99){
    		a=num/10;
    		b=num%10;
    		sum=a+b;
    		System.out.println("Sum is "+sum);
    		
    	}else{
    		return 0;
    	}
		
    	return sum;
    }
}
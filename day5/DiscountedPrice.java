package day5;

import java.util.Scanner;

public class DiscountedPrice {

	public static void main(String[] args) {
		int amount;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the amount");
		amount = sc.nextInt();
		System.out.println("The discounted price is "+Price(amount));
	}

	public static int Price(int amount) {
		int sp=0,discount;
		if (amount>0 && amount<=10000){
			discount= (amount*10) / 100;
			sp=amount-discount;
		}if (amount>10000 && amount<=20000){
			discount= (amount*20) / 100;
			sp=amount-discount;
		}if(amount>20000){
			discount= (amount*25) / 100;
			sp=amount-discount;
		}
		
		return sp;
	}

}

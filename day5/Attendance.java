package day5;

import java.util.Scanner;

public class Attendance {

	public static void main(String[] args) {
		float held,attended;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter number of classes held");
		held=sc.nextInt();
		System.out.println("Enter number of classes attended");
		attended=sc.nextInt();
       System.out.println( Percentage(held,attended));
		
	}

	public static String Percentage(float held, float attended) {
		float percentage;
		String s;
		percentage=(attended / held) * 100;
		System.out.println(percentage);
		if(percentage<75){
			
			s="Student not allowed to sit in exam";
		}
		else {
			s="Student is allowed to sit in exam";
		}
	
	return s;	
	}

}

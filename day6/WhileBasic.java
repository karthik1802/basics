package day6;

public class WhileBasic {

	public static void main(String[] args) {
       int num = 10;
       Loop(num);
	}

	public static void Loop(int num) {
         while(num > 0){

        	 System.out.println("Number is  "+num);
        	 num -=1;
         }
	}

}

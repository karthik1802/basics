package day6;

import java.util.Scanner;

public class NaturalNumbers {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a value of ");
		int n = sc.nextInt();
		Sum(n);

	}

	public static void Sum(int i) {
		int sum=0;
		while(i>0){
			sum=sum+i;
			i -=1;

		}
		System.out.println("Sum is " +sum);
	}

}

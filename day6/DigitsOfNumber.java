package day6;

import java.util.Scanner;

public class DigitsOfNumber {

	public static void main(String[] args) {
		int num;
		Scanner sc= new Scanner(System.in); 
		System.out.println("Enter a number ");
		num = sc.nextInt();
		DoN(num);
	}

	public static int DoN(int num) {
		int n = 0;
		while(num > 0){
			n = num % 10;
			System.out.println(n);
			num = num / 10;
		}
		return num;
	}

}

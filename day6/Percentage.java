package day6;

import java.util.Scanner;

public class Percentage {

	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter total marks of three subjects");
		float total=sc.nextFloat();
		System.out.println("Enter marks obtained in three subjects");
		float sub1 = sc.nextFloat();
		float sub2 = sc.nextFloat();
		float sub3 = sc.nextFloat();
		Percent(sub1,sub2,sub3,total);

	}

	public static void Percent(float sub1, float sub2, float sub3, float total) {
		float percent;
		percent = ((sub1+sub2+sub3)/total)*100;
		if(percent>=90){
			System.out.println("grade=A");
		}
		else if(percent<90){
			System.out.println("grade=B");
		}
		else if(percent<70){
			System.out.println("grade=c");
		}
		else{
			System.out.println("grade=F");
		}

	}
}



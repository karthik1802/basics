package day6;

import java.util.Scanner;

public class NextHundred01 {
	public static void main(String[] args) {
     Scanner sc=new Scanner(System.in);
     System.out.println("Enter a value");
     int num=sc.nextInt();
     
     System.out.println(getNextMultipleOf100(num));
	}
	
	public static int getNextMultipleOf100(int num) {
		int x,n;
		if(num>0){
			n = num/100;
			n +=1;
			x = n*100;
		}else{
			return -1;
		}
		return x;
	}
}

package day4;

import java.util.Scanner;

public class Evenodd {

	public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	
	System.out.println("Enter the value : ");
	int num = sc.nextInt();

	String evenodd = Evenodd(num);
	System.out.println(evenodd);
	
	}

	public static String Evenodd(int num) {
		String s;
        if((num%2)==0){
        	 s = "EVEN";
        }
        else{
        	 s = "ODD";
        }
		return s;
	}

}

import java.util.Scanner;

public class FibonacciAlpha {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter a word:");
		String str = scan.nextLine();
		String word = str.toUpperCase();
		System.out.println(getSum(word));

	}

	public static int getEachLetterValue(char ch) {

		int temp = 0;
		String s1 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		int value[] = new int[s1.length()];
		value[0] = 0;
		value[1] = 1;

		for (int i = 2; i < s1.length(); i++) {

			temp = value[i - 1] + value[i - 2];
			value[i] = temp;

		}
		return value[s1.indexOf(ch)];

	}

	public static int getSum(String s) {

		int sum = 0;
		for (int i = 0; i < s.length(); i++) {
			sum += getEachLetterValue(s.charAt(i));
		}
		return sum;
	}

}

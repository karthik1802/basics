package day7;

import java.util.Scanner;

public class Palindrome {

	public static void main(String[] args) {
		int x;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a number ");
		int num = sc.nextInt(); 

		x=num;
		if(x==Reverse(num)){
			System.out.println("It is a palindrome");
		}
		else{
			System.out.println("It is not a palindrome");
		}
	}
	public static int Reverse(int n) {
		int digit,reverse=0;
		while(n>0)
		{
			digit = n%10;
			reverse = reverse*10 + digit;
			n = n/10;
		}

		return reverse;
	}

}

package day7;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

/** Please don't make the class public **/
class Solution {

    public static void main(String[] args) {
       int x;
			Scanner sc=new Scanner(System.in);
			System.out.println("Enter a number ");
			int num = sc.nextInt(); 

			x=num;
			System.out.println(x);
			System.out.println(Reverse(num));
		}
		public static int Reverse(int n) {
			int digit,reverse=0;
			while(n>0)
			{
				digit = n%10;
				reverse = reverse*10 + digit;
				n = n/10;
			}

			return reverse;

		}

    }
    

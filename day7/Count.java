package day7;

import java.util.Scanner;

public class Count {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int num = sc.nextInt();

		System.out.println(CheckCount(num));

	}

	public static String CheckCount(int n) {
		int x,evencount = 0,oddcount=0;
		while(n>0){
			if(n%2==0){
				evencount++;
			}
			else{
				oddcount++;

			}
			x =n%10;
			n=n/10;
		}
		String s ="Even digits = " + evencount + "\n" + "Odd digits = " + oddcount;
		//	System.out.println("Count of odd numbers is " +oddcount);
		return s;
	}

}
package day3;

import java.util.Scanner;

public class Average {

	public static void main(String args[]){
		System.out.println("Enter three values");
		Scanner sc=new Scanner(System.in);

		float n1=sc.nextFloat();
		float n2=sc.nextFloat();
		float n3=sc.nextFloat();

		float avg=Average(n1,n2,n3);
		System.out.println("The average of numbers is "+ avg);


	}

	public static float Average(float a, float b, float c) {
		float average=(a+b+c)/3;
		return average;
	}
}

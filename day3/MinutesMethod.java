package day3;

import java.util.Scanner;

public class MinutesMethod {

	public static void main(String args[]){


		Scanner sc = new Scanner(System.in);
		System.out.print("Input the number of minutes: ");
		double min = sc.nextDouble();

		String tm = TimetoMin(min);
		System.out.println("The time is  " +tm);

	}

	public static String TimetoMin(double min) {
		long years = (long) (min / (60 * 24 * 365));
		int days = (int) (min / 60 / 24) % 365;
		int hours = (int) (min / 60) % 24;
		int minutes= (int)(min)% 60;
		String s =(int) min + " minutes is " + years + " years and " + days + " days and "+ hours +" hours and "+ minutes +" minutes";
		return s;

	}
}

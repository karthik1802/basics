package day3;

import java.util.Scanner;

public class CircleMethod {

	public static void main(String[] args) {
		System.out.println("Enter the radius of circle ");
		Scanner sc = new Scanner(System.in);
		float r = sc.nextFloat();

		double a = Area(r); 
		double p = Perimeter(r);

		System.out.println("The area of circle is "+ a);
		System.out.println("The perimeter of circle is "+ p);

	}

	public static double Area(float r) {
		double area = 3.14*r*r;

		return area;
	}

	public static double Perimeter(float r) {
		double perimeter = 2*3.14*r;

		return perimeter;
	}



}

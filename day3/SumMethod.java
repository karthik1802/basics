package day3;

import java.util.Scanner;

public class SumMethod {

	public static float sum(float a, float b){
	float sum= a+b;
		return sum;		
	}
	
	public static void main(String[] args) {
		
		float num1,num2, s;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter two values ");
		num1=sc.nextFloat();
		num2=sc.nextFloat();
		s=sum(num1,num2);
		System.out.println("The sum of two numbers is "+s);
	}

}

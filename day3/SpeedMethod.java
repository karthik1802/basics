package day3;

import java.util.Scanner;

public class SpeedMethod {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.print("Give the distance in meters: ");
		double distance = sc.nextInt(); 
		System.out.print("Give the time in hours: ");
		double hours = sc.nextInt();
		System.out.print("Give the time in minutes: ");
		double minutes = sc.nextInt(); 
		System.out.print("Give the time in seconds: ");
		double	seconds = sc.nextInt(); 

		double mps = MPS(distance,hours,minutes,seconds);
		double kmph = KMPH(distance,hours,minutes,seconds);
		double mph = MPH(distance,hours,minutes,seconds);

		System.out.println("The speed is " +mps+" m/s");
		System.out.println("The speed is " +kmph+" km/h");
		System.out.println("The speed is " +mph+" mile/h");
	}




	public static double MPS(double d,double hours,double minutes,double seconds) {

		double sec = tsec(hours, minutes, seconds);
		double speed1 = d/sec;
		return speed1;
	}


	public static double tsec(double hours,double minutes,double seconds) {


		double sec1 = hours * (3600);
		double sec2 = minutes * 60;
		double sec3 = seconds;
		double tsec = (sec1+sec2+sec3);

		return tsec;
	}

	public static double thr(double hours,double minutes,double seconds) {


		double sec = tsec(hours, minutes, seconds);
		double thr= sec * 0.000277778;

		return thr;
	}


	public static double KMPH(double d,double hours,double minutes,double seconds) {
		double km = (d * 0.001);
		double hr = thr(hours, minutes, seconds);

		double speed2 = km/hr;
		return speed2;
	}
	public static double MPH(double d,double hours,double minutes,double seconds) {
		double mile= (d * 0.0006213);
		double hr = thr(hours, minutes, seconds);
	
		double speed3 = mile/hr;
		return speed3;
	}



}
package day2;

import java.util.Scanner;

public class Speed {

	public static void main(String[] args) {
		double distance,hours,minutes,seconds;

		Scanner sc=new Scanner(System.in);
		System.out.print("Give the distance in meters: ");
		distance=sc.nextInt(); 
		System.out.print("Give the time in hours: ");
		hours=sc.nextInt();
		System.out.print("Give the time in minutes: ");
		minutes=sc.nextInt(); 
		System.out.print("Give the time in seconds: ");
		seconds=sc.nextInt(); 

		double sec1= hours * (3600);
		double sec2= minutes * 60;
		double tsec=(sec1+sec2+seconds);

		double mile= (distance * 0.0006213);
		double km= (distance * 0.001);

		//        double hr1=  (seconds * 0.000277778);
		//        double hr2= (minutes * 0.0166667);

		double hr= tsec * 0.000277778;
		
		
		double s1= distance /tsec;
		System.out.println("The speed is "+s1+ " m/s");

		float s2= (float) (km/hr);
		System.out.println("The speed is "+s2+ " km/hr");

		float s3= (float) (mile/hr);
		System.out.println("The speed is "+s3+ " mile/hr");
	}

}

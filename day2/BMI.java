package day2;

import java.util.Scanner;

public class BMI {

	public static void main(String[] args) {
		System.out.println("Enter the weight and height of body");
		Scanner sc=new Scanner (System.in);
		float weight= sc.nextFloat();
		float height= sc.nextFloat();
		double meters = height * 0.0254;
		
		double bmi= weight/(height*height);
		
		System.out.println("The Body Mass Index is " +bmi +"kg/m^2");
	}

}

package day2;

import java.util.Scanner;

public class Minutes {

	    public static void main(String[] Strings) {


	        double minutesInYear = 60 * 24 * 365;

	        Scanner input = new Scanner(System.in);

	        System.out.print("Input the number of minutes: ");

	        double min = input.nextDouble();

	        long years = (long) (min / minutesInYear);
	        int days = (int) (min / 60 / 24) % 365;
	        int hours = (int) (min / 60) % 24;
	        int minutes= (int)(min)% 60;
	        System.out.println((int) min + " minutes is " + years + " years and " + days + " days and "+ hours +" hours and "+ minutes +" minutes");
	    }
	}



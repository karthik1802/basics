package day2;
import java.util.Scanner;

public class ConversionIM {
	


	    public static void main(String[] Strings) {

	        Scanner input = new Scanner(System.in);

	        System.out.print("Input value of inch: ");
	        double inch = input.nextDouble();
	        double meters = inch * 0.0254;
	        System.out.println(inch + " inch is " + meters + " meters");

	    }
	}

package day11;

import java.util.Scanner;

public class WordCount {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println(("Enter a String: "));
		String str = sc.nextLine();
		int wc=1;
		System.out.println("Number of words: "+getWordCount(wc,str));
	}

	private static int getWordCount(int wc, String str) {
        for(int i=0;i<str.length();i++){
        	if(str.charAt(i)==' '){
        		wc++;
        	}
        }
        return wc;
	}
}

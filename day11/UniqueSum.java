package day11;

import java.util.Scanner;

public class UniqueSum {

	public static void main(String[] args) {
		int arr[]= new int[6];
		Scanner sc= new Scanner(System.in);
		for(int i=0;i<6;i++ ){
			arr[i]=sc.nextInt();
		}
		System.out.println("Sum: "+getUniqueSum(arr));


	}

	public static int getUniqueSum(int[] arr) {
		int sum=0;
		for(int i=0;i<arr.length;i++){
			int count=0;
			if(arr[i]!=-1){
				int e=arr[i];
				for(int j=0;j<arr.length;j++){
					if(e==arr[j]){
						count++;
						arr[j]=-1;
					}
				}
				if(count%2!=0){
					sum+=e;
					System.out.print(e+" ");
				}
			}
		}
		System.out.println();
		return sum;
	}

}

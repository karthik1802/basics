package day11;

import java.util.Scanner;

public class SwapCase {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println(("Enter a String: "));
		String str = sc.nextLine();
		System.out.println(getSwapCase(str));
	}

	private static String getSwapCase(String str) {
		String s="";
		for(int i=0;i<str.length();i++){
			char c = str.charAt(i);
			if(Character.isUpperCase(c)){
				c = Character.toLowerCase(c);
			}else if(Character.isLowerCase(c)){
				c = Character.toUpperCase(c);

			}
			s+=c;
		}
		return s;
	}

}

package day11;

import java.util.Scanner;

public class IdMatrix {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int matrix[][] = new int[3][3];
		System.out.println("Enter matrix elements ");
		for(int r=0;r<3;r++){
			for(int c=0;c<3;c++){
				matrix[r][c] = sc.nextInt();
			}
		}
		if(checkIdMatrix(matrix)){
			System.out.println("Identity Matrix");
		} else{
			System.out.println("Not Identity Matrix");
		}

	}

	private static boolean checkIdMatrix(int[][] matrix) {
		System.out.println("Array elements");
		for(int r=0;r<3;r++){

			for(int c=0;c<3;c++){
				if(r==c){
					if(matrix[r][c]!=1)
						return false;
				}else{
					if(matrix[r][c]!=0)
						return false;

				}
			}
		}

		return true;		
	}
}


package day11;


import java.util.Scanner;

public class IdentityMatrix {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int size=sc.nextInt();
		printIdMatrix(size);
	}

	private static void printIdMatrix(int size) {
		System.out.println("Array elements");
		for(int r=0;r<size;r++){
			for(int c=0;c<size;c++){
				if(r==c){
					System.out.print(1+" ") ;
				}
				else{
					System.out.print(0+" ");
				}
			}
			System.out.println();
		}
	}

}


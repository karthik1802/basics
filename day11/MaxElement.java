package day11;

import java.util.Scanner;

public class MaxElement {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int matrix[][] = new int[3][3];
		System.out.println("Enter matrix elements ");
		for(int r=0;r<3;r++){
			for(int c=0;c<3;c++){
				matrix[r][c] = sc.nextInt();
			}
		}
		getMaxElement(matrix);


	}

	private static void getMaxElement(int[][] matrix) {
		System.out.println("Array elements");
		for(int r=0;r<3;r++){
			int max=0;
			for(int c=0;c<3;c++){

				System.out.print(matrix[r][c]+" ");
				if(max<matrix[r][c]){

					max=matrix[r][c];				
				}

			}	
			System.out.println("   "+ max);
			System.out.println();
		}
	}

}

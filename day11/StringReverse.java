package day11;

import java.util.Scanner;

public class StringReverse {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println(("Enter a String: "));
		String str = sc.nextLine();
		System.out.println("Given String :"+str);
		String s=str;
		System.out.println("Reversed String is: " +reverseString(str));

		if(s.equals(reverseString(str))){
			System.out.println("Yes");
		}else{
			System.out.println("No");
		}

	}

	private static String reverseString(String str) {
		String reverse="";
		for(int i=str.length()-1;i>=0;i--){
			reverse=reverse + str.charAt(i);
		}
		return reverse;

	}

}

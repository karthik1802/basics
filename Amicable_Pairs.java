public class Amicable_Pairs {

	public static void main(String[] args) {

		System.out.println(getSixDigitAmicablePairs());

	}

	public static String getSixDigitAmicablePairs() {
		String res = "";
		for (int i = 100000; i <= 999999; i++) {
			int temp = getAliquot(i);
			if (getAliquot(temp) == i) {
				if (i < temp) {
					res += "(" + i + "," + temp + ")\n";
				}
			}
		}
		return res;
	}

	public static int getAliquot(int num) {
		int sum = 1;
		for (int i = 2; i * i < num; i++) {
			if (num % i == 0)
				sum += i + (num / i);
			if (i * i == num)
				sum += i;
		}

		return sum;
	}

}
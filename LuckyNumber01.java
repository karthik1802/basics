public class LuckyNumber01 {
    public static void main(String[] args) {
        String date = "15-March-2016";
        System.out.println(getLuckyNumber(date));
    }

    public static int getLuckyNumber(String date) {
    	String[] partsOfDate=date.split("-");
    	int dd=Integer.parseInt(partsOfDate[0]);
    	int mm=convertMMMtoMM(partsOfDate[1]);
    	int year=Integer.parseInt(partsOfDate[2]);
    	
          int sum_of_digits= getSumOfDigits(dd);
          sum_of_digits+= getSumOfDigits(mm);
          sum_of_digits+= getSumOfDigits(year);
          
          
          while(sum_of_digits>10){
        	  sum_of_digits =getSumOfDigits(sum_of_digits);
          }
          return sum_of_digits;
    }

    public static int convertMMMtoMM(String mon) {
    	 String months="JANFEBMARAPRMAYJUNJULAUGSEPOCTNOVDEC";
  	   mon = mon.substring(0,3);
  	   mon = mon.toUpperCase();
  	   
  	 return ((months.indexOf(mon)/3)+1);
  	   
    }
    
    public static int getSumOfDigits(int num) {
    	int sum=0;
    	while(num>0){
    	 sum += num%10;
    	 num/=10;
    		
    	}
    	return sum;
    }

}

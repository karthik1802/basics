import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Collatz2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int num = sc.nextInt();
		getCollatzSequence(num);
	}

	public static void getCollatzSequence(int number) {
		List<Integer> Collatz = new ArrayList<Integer>();
		while (number != 1) {
			Collatz.add(number);
			if (number % 2 == 0) {
				number /= 2;
			} else {
				number = (number * 3) + 1;
			}
			if (number == 1)
				Collatz.add(1);

		}
		System.out.println(Collatz);
		int max = 0, value = 0;
		int i;
		for (Integer c : Collatz) {
			int count = 0;
			i=c;
			while(c%2==0){
				count++;
				c=c/2;
			}
			if(max<count){
				max=count;
				value=i;
			}
			
//			System.out.println(value + " " + max);
		}
		System.out.println(value);
			
			
	}

}

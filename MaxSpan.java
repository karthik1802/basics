import java.util.Arrays;
import java.util.HashMap;

public class MaxSpan {
	public static int findMaxFrequency(int[] arr) 
	{
		int count = 1, tempCount;
		int popular = arr[0];
		int temp = 0;
		int i=0;
	
		
		for ( i = 0; i < (arr.length - 1); i++)
		{
			
			
			temp = arr[i];
			tempCount = 0;
			for (int j = 1; j < arr.length; j++)
			{
				if(arr[i]<=0)
					return -1;
				
				if(arr[i]!=(int)(arr[i]))
					return 999;
				
				
				if (temp == arr[j])
					tempCount++;
			}
			if (tempCount > count)
			{
				popular = temp;
				count = tempCount;
			}
		}
		return popular;
	}


	public static void main(String[] args) {
		int[] arr = { 1,1,1,1,1,1, 2, 2, 3,3,3,3};
		System.out.println(findMaxFrequency(arr));
	}
}

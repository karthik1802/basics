public class MaskEmailID01 {
    public static void main(String[] args) {
        String email = "testmail@.mailme.com";
        System.out.println(maskMailID(email));
    }

    public static String maskMailID(String email) {
       int index = email.indexOf("@");
       //System.out.println(index);
       String endId = email.substring(index);
       //System.out.println(endId);
       String mask = "";
       int size = email.substring(0, index).length();
       
       for(int i = 2;i < size; i++)
    	   mask +="X";
       
       
       mask = email.substring(0,2) + mask + endId;
       
       return mask;
       
       
       
    }

}

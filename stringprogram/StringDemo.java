package stringprogram;

public class StringDemo {

	public static void main(String[] args) {
		String name = "John Doe";
		String date="18-March-2018";
		
		System.out.println(name.charAt(0));
		System.out.println(name.length());
		System.out.println(name.substring(3));
		//System.out.println(name.indexOf(" "));
		System.out.println(name.substring(0,name.indexOf(" ")));
		System.out.println(name.substring(name.indexOf(" ")+1));
		System.out.println(date.substring(date.indexOf("-")+1, date.lastIndexOf("-")));
		String s[]=date.split("-");
		System.out.println(s[1]);
	}

}

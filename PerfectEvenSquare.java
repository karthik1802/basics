import java.util.Scanner;

public class PerfectEvenSquare {
	public static void main(String args[]) {
	
int start= 1000;
int limit= 9999;
		System.out.println(getPerfectEvenSquare(start,limit));

	}

	private static String getPerfectEvenSquare(int start, int limit) {
		String s = "";
		for (int i = start; i <= limit; i++) {
			if (getPerfectSquare(i) && getEvenNumber(i)) {
				s = s + i + " ";
			}

		}
		return s;
	}

	private static boolean getEvenNumber(int num) {
		int digit = 0;
		if (num <= 0) {

			return false;
		} else {
			while (num >= 1) {
				digit = num % 10;
				if (digit != 0) {
					if (digit % 2 != 0) {
						return false;
					}
				} else {
					return false;
				}
				num /= 10;
			}
			return true;
		}

	}

	private static boolean getPerfectSquare(int i) {

		int sqrt = (int) Math.sqrt(i);
		if (sqrt * sqrt == i)
			return true;

		return false;
	}

}

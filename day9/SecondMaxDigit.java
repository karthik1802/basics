package day9;

import java.util.Scanner;

public class SecondMaxDigit {

	public static void main(String[] args) {
	 long  num;
	 Scanner sc= new Scanner(System.in);
	 num = sc.nextLong();
	 System.out.println(SecondMax(num));
	 
	}

	public static long SecondMax(long num) {
      int max=0,secondMax=0;
      while(num>0){
    	  int digit =(int) (num%10);
    	  if(digit>max){
    		  secondMax=max;
        	  max=digit; 
    	  }else if(digit<max && digit>secondMax){
    		  secondMax=digit;
    	  }
    	 num=num/10;
      }
	return secondMax;
	}

}

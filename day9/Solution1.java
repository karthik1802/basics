package day9;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class Solution1 {



    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int N = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
        Multiple(N);
        scanner.close();
    }

	public static void Multiple(int n) {
		int i=1,m=0;
		for(i=1;i<10;i++){
			m=n*i;
			System.out.println(n+" x "+i+" = "+m);
		}
	}
}

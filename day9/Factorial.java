package day9;

import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		System.out.println(Fact(n));
	}

	public static int Fact(int n) {
		int fact = 1;
		for(int i=1;i<=n;i++){
		 fact = n*Fact(n-1);
		}
		return fact;
	}

}

package day9;

import java.util.Scanner;

public class Prime {

	public static void main(String[] args) {


		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a number : ");
		int num=sc.nextInt();

		if(CheckPrime(num)){
			System.out.println("It is  Prime");
		}else{
			System.out.println(("It is not Prime number"));
		}

	}

	public static boolean CheckPrime(int n) {
		for(int i=2;i<n;i++){
			if(n%i==0)
				return false;
		}
		return true;

	}

}

package day10;

import java.util.Scanner;

public class Sorting {

	public static void main(String[] args) {
		int n;

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter size of array: ");
		n=sc.nextInt();
		int arr[]= new int[n];
		System.out.println("Enter array elements");
		for(int i=0;i<n;i++ ){
			arr[i]=sc.nextInt();
		}
		System.out.println("Before Sorting :");
		printArray(arr);
		System.out.println("After Sorting :");
		printArray(ascendingSort(arr));
	}

	private static void printArray(int[] arr) {
		for(int i=0;i<arr.length;i++ ){
			System.out.print(arr[i]+ " ");
		
		}System.out.println();	
	}

	public static int[] ascendingSort( int[] arr) {
		for(int i=0;i<arr.length;i++){
			for(int j=i;j<arr.length;j++){
				if(arr[i]>arr[j]){
				arr[i]=arr[i]+arr[j];
				arr[j]=arr[i]-arr[j];
				arr[i]=arr[i]-arr[j];
				}
			}
		}
		return arr;
	}

}

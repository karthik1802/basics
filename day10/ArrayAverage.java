package day10;
import java.util.Scanner;

public class ArrayAverage {

	public static void main(String[] args) {
		 int arr[]= new int[6];
		 Scanner sc= new Scanner(System.in);
		 
		 for(int i=0;i<6;i++)
			 arr[i]=sc.nextInt();
		 System.out.println(getAverage(arr));
	}

	public static double getAverage(int[] arr) {
		int sum=0;
		double average=0;
		for(int i=0;i<arr.length;i++)
			sum+=arr[i];
		average=sum / arr.length;
		
		return average;
	}

}

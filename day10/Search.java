package day10;

import java.util.Scanner;

public class Search {

	public static void main(String[] args) {
		int arr[]= new int[6];
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter array elements");
		for(int i=0;i<6;i++ ){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter search element ");
		int n=sc.nextInt();
		int index = Found(n,arr);
		if(index!=0){
			System.out.println("Element found at position " +index );

		}
		else{
			System.out.println("Element not found");
		}
	}

	public static int Found(int n, int[] arr) {
		for(int i=0;i<arr.length;i++){
			if(arr[i]==n)
				return i;
		}
		return -1;
	}

}

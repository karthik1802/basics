package day10;

public class OddOccurence {

	public static void main(String[] args) {
		int arr[]={4,6,3,1,5,3,6,9};
		System.out.println("count: "+getOddCount(arr));

	}

	public static int getOddCount(int[] arr) {
		int oddcount=0;
		for(int i=0;i<arr.length;i++){
			int count=0;
			if(arr[i]!=-1){
				int e=arr[i];
				for(int j=0;j<arr.length;j++){
					if(e==arr[j]){
						count++;
						arr[j]=-1;
					}
				}
				if(count%2!=0){
					oddcount++;
					System.out.print(e+" ");
				}
			}
		}
		System.out.println();
		return oddcount;
	}

}

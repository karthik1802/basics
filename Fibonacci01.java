import java.util.Scanner;

public class Fibonacci01 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int n= sc.nextInt();
		System.out.println(getFibonacciSeries(n));
	}
	public static String getFibonacciSeries(int num) {
		int i=1,t1=0,t2=1;
		String s ="";
		if(num<1 || num>40)
			return "-1";

		while (i <= num)
		{
//			s += t1 +",";
			if(i==num){
				s+=t1;
			}else{
				s+=t1+",";
			}
			int  sum = t1 + t2;
			t1 = t2;
			t2 = sum;
			i++;
		}

		//		s=t1+","+t2+",";
		//		for(i=1;i<=num-2;++i){
		//
		//			sum = t1+t2;
		//			t1=t2;
		//			t2=sum;
		//			if(i==num-2){
		//				s +=sum;
		//			}else{
		//				s +=sum + ",";
		//			}
		//
		//		}
		return s;


	}
}
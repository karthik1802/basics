public class Fibonacci_NthTerm {

	public static void main(String[] args) {
		System.out.println(getNthTermOfFibonacciSeries(5));
	}

	public static int getNthTermOfFibonacciSeries(int n) {
		int t1=0,t2=1,sum=0;
		//		System.out.print("First " + n + " terms: ");
		if(n<=0)
			return -1;

		for (int i = 1; i < n; ++i)
		{
	//System.out.print(t1 + " ");

			sum = t1 + t2;
			t1 = t2;
			t2 = sum;
		}		
		return t1 ;
		// ADD YOUR CODE HERE
	}


}
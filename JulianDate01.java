public class JulianDate01 {
    public static void main(String[] args) {
        String date = "23-Jan-2016";
        System.out.println(dateFormat(date));
    }
   
    public static String dateFormat(String date) {
    	String[] partsOfDate=date.split("-");
    	int dd=Integer.parseInt(partsOfDate[0]);
    	int mon=convertMMMtoMM(partsOfDate[1]);
    	int yyyy=Integer.parseInt(partsOfDate[2]);
		return yyyy + julianDate(dd, mon);
    }
   
   public static String julianDate(int dd, int mon) {
	   int[] MONTHS = {0,31,59,90,120,151,181,212,243,273,304,334,365};
	   int j = MONTHS[mon-1] + dd;
	   String day = ""+j;
	   if(j<=9)
		   day= "00"+j;
	   else if(j<=99)
		   day = "0"+ j;
	   
	   return day;
	   }
   
   public static int convertMMMtoMM(String mon) {
	   String months="JANFEBMARAPRMAYJUNJULAUGSEPOCTNOVDEC";
	   mon = mon.substring(0,3);
	   mon = mon.toUpperCase();
	   
	   return ((months.indexOf(mon)/3)+1);
	   }
}

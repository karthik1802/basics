public class TwinPrimes01 {
	public static void main(String[] args) {
		int num1 = 1;
		int num2 = 200;
		System.out.println(twinPrimes(num1, num2));
	}

	public static String twinPrimes(int start, int limit) {
		if(start<=0 || limit<=0)
			return "-1";
		
		if(start>=limit)
			return "-2";
		
		String s="";
		int least=2;
		for(int i=start;i<=limit;i++){
			if(isPrime(i)){
				if(i-least==2){
					s+= least + ":" + i + ",";
				}
				least=i;
			}
		}
		if(s.length()==0){
			return "-3";
		}
		s=s.substring(0,s.length()-1);
		return s;

	}

	public static boolean isPrime(int num) {
		if(num==1)
			return false;
		if(num==2)
			return true;
		if (num%2==0)
			return false;

		for(int i=2;i<num;i++){
			if(num%i==0)
				return false;
		}

		return true;
	}
}
